import babel from "rollup-plugin-babel";
import commonjs from "rollup-plugin-commonjs";
import resolve from "rollup-plugin-node-resolve";
import pkg from "./package.json";

const plugins = [
  resolve(),
  commonjs(),
  babel({
    presets: [["es2015", {modules: false}]],
    plugins: ["external-helpers"],
    babelrc: false,
    exclude: "node_modules/**",
  }),
];

export default [
  {
    input: "lib/index.js",
    output: {
      file: pkg.main,
      format: "cjs",
      sourcemap: true,
    },
    name: "scaffold",
    external: ["lodash/fp"],
    plugins,
  },
];
