import {size} from "lodash/fp";

/**
 * A plugin for Littlefork.
 *
 * Does cool stuff.
 */
const plugin = (envelope, {log}) => {
  log.info(`Calling a plugin with ${size(envelope.data)} units.`);

  return envelope;
};

// Give an one line description of this plugin.
plugin.desc = "A plugin boilerplate module.";

// Set plugin specific config options in the yargs format.
plugin.argv = {};

// A Markdown multi line description of the plugin.
plugin.doc = `
Place a multi
lint description in **Markdown**.
`;

export default plugin;
